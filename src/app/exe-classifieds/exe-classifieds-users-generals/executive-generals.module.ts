import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module' 
import { ExeClassifiedsUsersGeneralsComponent } from './exe-classifieds-users-generals.component';
import { ExecutiveGeneralsService } from './executive-generals.service';

const routes:Routes=[{path:'exeClientsGeneral/:_id/:name',component:ExeClassifiedsUsersGeneralsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [ExeClassifiedsUsersGeneralsComponent],
  providers: [ExecutiveGeneralsService],

})
export class ExecutiveGeneralsModule { }
