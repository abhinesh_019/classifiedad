import { TestBed, inject } from '@angular/core/testing';

import { ExeClassifiedsDetailsService } from './exe-classifieds-details.service';

describe('ExeClassifiedsDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExeClassifiedsDetailsService]
    });
  });

  it('should be created', inject([ExeClassifiedsDetailsService], (service: ExeClassifiedsDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
