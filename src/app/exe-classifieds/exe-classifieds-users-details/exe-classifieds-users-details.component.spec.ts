import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExeClassifiedsUsersDetailsComponent } from './exe-classifieds-users-details.component';

describe('ExeClassifiedsUsersDetailsComponent', () => {
  let component: ExeClassifiedsUsersDetailsComponent;
  let fixture: ComponentFixture<ExeClassifiedsUsersDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExeClassifiedsUsersDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExeClassifiedsUsersDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
