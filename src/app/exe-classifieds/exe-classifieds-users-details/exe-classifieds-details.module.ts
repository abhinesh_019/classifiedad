import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module' 
 import { ExeClassifiedsDetailsService } from './exe-classifieds-details.service';
import { ExeClassifiedsUsersDetailsComponent } from './exe-classifieds-users-details.component';

const routes:Routes=[{path:'exeDetails/:_id/:name',component:ExeClassifiedsUsersDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [ExeClassifiedsUsersDetailsComponent],
  providers: [ExeClassifiedsDetailsService],

})
export class ExeClassifiedsDetailsModule { }
