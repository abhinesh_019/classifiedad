import { Component, OnInit } from '@angular/core';
import { ExeClassifiedsService } from './exe-classifieds.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exe-classifieds',
  templateUrl: './exe-classifieds.component.html',
  styleUrls: ['./exe-classifieds.component.css']
})
export class ExeClassifiedsComponent implements OnInit {
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public localAreasId:any
  public localAreaData:any
public localAreaResults:any
public imagesCat:any
public catageriesLocalsData:any
public catageriesId:any
public catageriesd:any
public subCatageriesData:any
public subCatageriesId:any
public subCatageries:any
public datesGetData:any
public datesData:any
public datesId:any
public showmains:boolean=false
public selectedId:any
public executivesFieldsIds:any
public imagesOnes:any
public executivesFields:any
public executivesFieldsDeleteId:any
public executivesFieldsId:any
public executivesDelete:any
 public executivesDeleteId:any
public executivesPersons:any
public statusOneGetDetails:any
public statusOneDeleteId:any
public statusOnesId:any
public statusOnesName:any
public statusPoneId:any
public detailId:any
public catGroup:any
public areasAllGroup:any
public searchString:any
public executivesPersonsId:any
public aboutUsgets:any
public blogsgets:any
public areaID:any
public emailIds:any
public contactNo:any
public userName:any


constructor(private classifieds:ExeClassifiedsService,private router:Router,) { }

  ngOnInit() {
    this.areaID=localStorage.getItem('adminsId');
    this.emailIds=localStorage.getItem('email');
    this.contactNo=localStorage.getItem('contactNo');
    this.userName=localStorage.getItem('userName') 
 
    this.localAreas()
    this.getAboutBlogs()
     this.getBlogs()
     this.getCatageriesLocals()
     this.executivesGet()
  }
  signOut(){

    this.router.navigate(["/login"]);

  }
  ClearData(){
    localStorage.removeItem("adminsId");
    localStorage.removeItem("email");
    localStorage.removeItem("contactNo");
    localStorage.removeItem("userName");
    this.router.navigate(["/login"]);

   }
  getAboutBlogs(){
    this.classifieds.aboutUsget().subscribe((res)=>{
    this.aboutUsgets=res      
    }) 
   }
 
    
    localAreas(){
      this.areaID=localStorage.getItem('adminsId');
 
      this.classifieds.Localareaapi(this.areaID).subscribe((res)=>{
        this.localAreaData=res
          
      })
     
    }
    
  

       getCatageriesLocals(){
        this.areaID=localStorage.getItem('adminsId');

        var data:any =  {}
        if(this.searchString){
          data.search=this.searchString
          }
        this.classifieds.getCatageriesLocal(this.areaID,data).subscribe((res)=>{
           this.catageriesLocalsData=res
            
         })
      }
      searchFilter(){
        this.getCatageriesLocals()
        
      }
    //  ************************* EXECUTIVES PERSONS **************************
    executivesGet(){
      this.classifieds.executivesGets(this.areaID).subscribe((res)=>{
        this.executivesPersons=res
          
        var id =this.executivesPersons[0]; 
         
    
        this.executivesExecutiveFieldsGet(id)
       })
    
    }
    executivesExecutiveFieldsGet(get){
      this.executivesFieldsIds=get._id
      localStorage.setItem("Executive  Id",this.executivesFieldsIds);

      this.classifieds.executivesExecutiveFieldsGets(this.executivesFieldsIds).subscribe((res)=>{
        this.executivesFields=res
        var id =this.executivesFields[0]    
       })
    }
    getBlogs(){
      this.classifieds.blogsget().subscribe((res)=>{
      this.blogsgets=res      
      })  }
  
}

