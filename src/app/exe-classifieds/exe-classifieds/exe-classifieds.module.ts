import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { ExeClassifiedsComponent } from './exe-classifieds.component';
import { ExeClassifiedsService } from './exe-classifieds.service';
import { ExecutiveGeneralsModule } from '../exe-classifieds-users-generals/executive-generals.module';
import { ExeClassifiedsDetailsModule } from '../exe-classifieds-users-details/exe-classifieds-details.module';

const routes: Routes = [
  {
    path: '', component: ExeClassifiedsComponent, children:
      [
      { path: 'exeClientsGeneral/:_id/:name', loadChildren: 'app/exe-classifieds/exe-classifieds-users-generals/executive-generals.module#ExecutiveGeneralsModule'},
      { path: 'exeDetails/:_id/:name', loadChildren: 'app/exe-classifieds/exe-classifieds-users-details/exe-classifieds-details.module#ExeClassifiedsDetailsModule'},

    ]
  }]
  

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule, 
    ExecutiveGeneralsModule,
    ExeClassifiedsDetailsModule
  ],
  declarations: [ExeClassifiedsComponent],
  providers: [ExeClassifiedsService],

})
export class ExeClassifiedsModule { }
