import { TestBed, inject } from '@angular/core/testing';

import { ExeClassifiedsService } from './exe-classifieds.service';

describe('ExeClassifiedsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExeClassifiedsService]
    });
  });

  it('should be created', inject([ExeClassifiedsService], (service: ExeClassifiedsService) => {
    expect(service).toBeTruthy();
  }));
});
