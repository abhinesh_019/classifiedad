import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExeClassifiedsComponent } from './exe-classifieds.component';

describe('ExeClassifiedsComponent', () => {
  let component: ExeClassifiedsComponent;
  let fixture: ComponentFixture<ExeClassifiedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExeClassifiedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExeClassifiedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
