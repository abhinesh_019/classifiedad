import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MaterialsModule } from '../shared/materials.module';
import { ResetpasswordComponent } from './resetpassword.component';

const routes:Routes=[{path:'reset-password/:token',component:ResetpasswordComponent},]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MaterialsModule
  ],
  declarations: [ResetpasswordComponent]
})
export class ResetpasswordModule { }
