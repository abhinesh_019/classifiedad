import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GolbalService } from '../golbal.service';
import { ActivatedRouteSnapshot,ActivatedRoute} from '@angular/router';
import { SweetalertService } from '../../sweetalert.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {
public message:any

  constructor(private router:Router,private loginservices:GolbalService,private route:ActivatedRoute,private alert:SweetalertService) { }
public fpass;

forgetpasswords={
  email:""
}
  ngOnInit() {
   
  }

  fgetpassword(){
    this.loginservices.forgetpassword(this.forgetpasswords).subscribe((res)=>{
      
      console.log(res);
      this.forgetpasswords.email=""
      this.alert.forgetPassword();


      
        })
      
      
  }
}
