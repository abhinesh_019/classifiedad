import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment.prod';
 
@Injectable()
export class GolbalService {


  constructor( private http: HttpClient) { }

  
signUpService(signup) {
    
  return this.http.post(environment.apiUrl+"/singup",signup)
}

loginService(login) {
    
  return this.http.post(environment.apiUrl+"/login",login)

}

forgetpassword(forgetpasswords) {
    
  return this.http.post(environment.apiUrl+"/forgetPassword",forgetpasswords,{})

}
rgetpassword(resetPasswordWithToken){
  return this.http.post(environment.apiUrl+"/resetPasswordWithToken",resetPasswordWithToken)

}
}

