import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../login/login.component';
import{ RouterModule,Routes, ActivatedRoute} from'@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';

import {MatInputModule} from '@angular/material/input';
import { GolbalService } from '../golbal.service';
import { SweetalertService } from '../../sweetalert.service';

const routes:Routes=[{path:'',component:LoginComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  


  ],
  declarations: [
    LoginComponent,

  ],
  providers: [GolbalService,SweetalertService],
})
export class LoginModule { }

