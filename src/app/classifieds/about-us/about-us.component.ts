import { Component, OnInit } from '@angular/core';
import { AboutusService } from './aboutus.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  public aboutUsgets:any;
public deleteId:any;


  formData: FormData = new FormData(); 
public imagesName:any
public imagesNames:any
public blogsgets:any
public deleteIds:any
public feedsGet:any
public classifiedExe:any
public executivesPersonsId:any
constructor(private abouts:AboutusService) { }

ngOnInit() {
  this.getAboutBlogs()
  this.getBlogs()
  this.executivesGet()
}
executivesGet(){
  this.classifiedExe=localStorage.getItem('Executive  Id');
  this.abouts.executivesGets(this.classifiedExe).subscribe((res)=>{
    this.executivesPersonsId=res
     
      })

}
getAboutBlogs(){
  this.abouts.aboutUsget().subscribe((res)=>{
  this.aboutUsgets=res      
  }) 
 }
 getBlogs(){
  this.abouts.blogsget().subscribe((res)=>{
  this.blogsgets=res      
  })  }
 

}
