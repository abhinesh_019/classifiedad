import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()


export class AboutusService {

  constructor(private http: HttpClient) { }
 
 aboutUsget(){
  return this.http.get(environment.apiUrl +"/clasdifiedsAboutUs");
              }

              blogsget(){
                return this.http.get(environment.apiUrl +"/clasdifiedsBlogs");
                            }

              feedbackPosts(data) {
                return   this.http.post(environment.apiUrl +"/clasdifiedsFeedbacks",data);
            
             
                }
                executivesGets(id){
                  return this.http.get(environment.apiUrl +"/classifidesExecutives/"+id);
                 }
}
