import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module' ;
import { AboutUsComponent } from './about-us.component';
import { AboutusService } from './aboutus.service';

const routes:Routes=[{path:'AboutUs',component:AboutUsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [AboutUsComponent],
  providers: [AboutusService],

})
export class AboutusModule { }
