import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module' 
 import { ClassifiedsDatesService } from './classifieds-dates.service';
import { ClassifiedsdatesComponent } from './classifiedsdates.component';


const routes:Routes=[{path:'imagesViews/:_id/:name',component:ClassifiedsdatesComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [ClassifiedsdatesComponent],
  providers: [ClassifiedsDatesService],

})
export class ClassifiedsDatesModule { }
