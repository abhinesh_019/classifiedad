import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiedsdatesComponent } from './classifiedsdates.component';

describe('ClassifiedsdatesComponent', () => {
  let component: ClassifiedsdatesComponent;
  let fixture: ComponentFixture<ClassifiedsdatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiedsdatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiedsdatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
