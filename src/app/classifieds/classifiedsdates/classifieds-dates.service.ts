import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class ClassifiedsDatesService {

  constructor(private http: HttpClient) { }

  aboutUsget(){
    return this.http.get(environment.apiUrl +"/clasdifiedsAboutUs");
                }
  clientsVidCatId(id){
    return this.http.get(environment.apiUrl +"/classifidesCatVid/"+id);
   }
   clientsCatVidD(id){
    return   this.http.delete(environment.apiUrl +"/classifidesCatVid/"+id);
   }
   clientsImgId(id){
    return this.http.get(environment.apiUrl +"/classifidesDetailsImgId/"+id);
   }


   clientsImgCatIds(id){
    return this.http.get(environment.apiUrl +"/classifidesCatImg/"+id);
   }

   clientsImgd(id){
    return   this.http.delete(environment.apiUrl +"/classifidesCatImg/"+id);
   }

   executivesGets(id){
    return this.http.get(environment.apiUrl +"/classifidesExecutives/"+id);
   }
   blogsget(){
    return this.http.get(environment.apiUrl +"/clasdifiedsBlogs");
                }

  feedbackPosts(data) {
    return   this.http.post(environment.apiUrl +"/clasdifiedsFeedbacks",data);

 
    }
}
