import { Component, OnInit } from '@angular/core';
 import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { ClassifiedsDatesService } from './classifieds-dates.service';

@Component({
  selector: 'app-classifiedsdates',
  templateUrl: './classifiedsdates.component.html',
  styleUrls: ['./classifiedsdates.component.css']
})
export class ClassifiedsdatesComponent implements OnInit {
public ids:any
public clientsCatImgs:any
public clientsCatVidRes:any
public clientsCatImgsId:any
public clientsImgsId:any
public detailsImgId:any
public detailsImgDelete:any
public detailsVidId:any
public detailsVidDelete:any
public aboutUsgets:any
public blogsgets:any
public classifiedExe:any
public executivesPersonsId:any

  public formData: any = new FormData();
  constructor(private classifieds:ClassifiedsDatesService,private route:ActivatedRoute) { }
  
    ngOnInit() {
   this.individualdata() 
   this.clientsImgId()
    this.getAboutBlogs()
  this.getBlogs()
  this.executivesGet()
}
executivesGet(){
  this.classifiedExe=localStorage.getItem('Executive  Id');
  this.classifieds.executivesGets(this.classifiedExe).subscribe((res)=>{
    this.executivesPersonsId=res
     
      })
    }
    individualdata(){
      this.ids=this.route.snapshot.params['_id'];    
   }
 

   clientsImgId(){
    this.classifieds.clientsImgId(this.ids).subscribe((res)=>{
      
      this.clientsImgsId=res 
console.log(res);

     })
  
  }
 


  
 
  getBlogs(){
    this.classifieds.blogsget().subscribe((res)=>{
    this.blogsgets=res      
    })  }
  getAboutBlogs(){
    this.classifieds.aboutUsget().subscribe((res)=>{
    this.aboutUsgets=res      
    }) 
   }
}

