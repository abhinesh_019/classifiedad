import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiedsdetailsComponent } from './classifiedsdetails.component';

describe('ClassifiedsdetailsComponent', () => {
  let component: ClassifiedsdetailsComponent;
  let fixture: ComponentFixture<ClassifiedsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiedsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiedsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
