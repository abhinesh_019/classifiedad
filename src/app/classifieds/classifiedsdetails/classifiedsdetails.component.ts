import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClassifiedsDetailsService } from './classifieds-details.service';

@Component({
  selector: 'app-classifiedsdetails',
  templateUrl: './classifiedsdetails.component.html',
  styleUrls: ['./classifiedsdetails.component.css']
})
export class ClassifiedsdetailsComponent implements OnInit {
  public ids:any
  public datesGetIds:any
  public imagesOne:any
  public executivesFields:any
  public executivesFieldsId:any
   public executivesDelete:any
   public executivesDeleteId:any
  public executivesPersons:any
  public selectedId:any
  public executivesFieldsIds:any
  public imagesOnes:any 
  public clientsCatImgs:any
  public getViewsClientsImgId:any
  public getViewsClientsImg:any
  public clientsImgs:any
  public clientsCatVidRes:any
  public detailsImgId:any
  public detailsImgDelete:any
  public aboutUsgets:any
  public blogsgets:any
  public showmains:boolean=false
   public classifiedExe:any
  

clients:{
  "name":"",
  "images":"",
  "description":""
}
clientsImg={
  "mainTitle":"",
  "images1":""
}


clientsCatImg={
  "title":"",
  "images":""
}

clientsCatVid={
  "title":"",
  "videos":""
}


  
public formData: any = new FormData();
constructor(private classifieds:ClassifiedsDetailsService,private route:ActivatedRoute) { }

  ngOnInit() {
 this.individualdata()
 this.getAboutBlogs()
 this.datesGetId()
 this.clientsImgGet()
 this.getBlogs()
 this.clientsImgCat()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];    
 }
 getAboutBlogs(){
  this.classifieds.aboutUsget().subscribe((res)=>{
  this.aboutUsgets=res      
  }) 
 }
 datesGetId(){
  this.classifieds.datesGets(this.ids).subscribe((res)=>{
    
    this.datesGetIds=res
    this.executivesGet()
  })

}
 

// ********************* Customer Details ****************

handleFileInput($event:any,images){
  this.imagesOne= File = $event.target.files[0];
 }


postDetailsLocals(){
this.formData.append('images',this.imagesOne);

this.formData.append('name',this.clients.name); 
this.formData.append('description',this.clients.description); 

  
 this.classifieds.executivesp(this.ids,this.formData).subscribe((res)=>{
     })
     this.clients.name="",
      this.clients.description="",
     this.clients.images=""
}
executivesGet(){
  this.classifiedExe=localStorage.getItem('Executive  Id');
  this.classifieds.executivesGetsId(this.classifiedExe).subscribe((res)=>{
    this.executivesPersons=res
     
      })

}
 
executivesExecutiveFieldsGet(get){
  this.executivesFieldsIds=get._id
  this.classifieds.executiveFieldsGets(this.executivesFieldsIds).subscribe((res)=>{
    this.executivesFields=res
    var id =this.executivesFields[0]    
   })
}

// ********************* Clients Images ****************
 

clientsImgGet(){
  this.classifieds.clientsImgGetIds(this.ids).subscribe((res)=>{
    
    this.clientsImgs=res
    var id=this.clientsImgs[0]
    })

}

 
// ********************* Clients Cat Images ****************
 

clientsImgCat(){
  this.classifieds.clientsImgCatIds(this.ids).subscribe((res)=>{
    
    this.clientsCatImgs=res
    var id=this.clientsCatImgs[0]

   })

}
 
getBlogs(){
  this.classifieds.blogsget().subscribe((res)=>{
  this.blogsgets=res      
  })  }
 


  
}
