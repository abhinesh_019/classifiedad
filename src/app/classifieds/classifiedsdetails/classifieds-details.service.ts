import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class ClassifiedsDetailsService {

  constructor(private http: HttpClient) { }
  datesGets(id){

    return this.http.get(environment.apiUrl +"/classifidesDetailsId/"+id);
  }
  aboutUsget(){
    return this.http.get(environment.apiUrl +"/clasdifiedsAboutUs");
                }

  executivesp(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesExecutives/"+id,data);
   }
   executivesGetsId(id){
    return this.http.get(environment.apiUrl +"/classifidesExecutivesId/"+id);
   }
   executiveFieldsGets(id){
    return this.http.get(environment.apiUrl +"/classifidesExecutivesFields/"+id);
   }



   ClientsImgp(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesDetailsImg/"+id,data);
   }
   clientsImgGetIds(id){
    return this.http.get(environment.apiUrl +"/classifidesDetailsImg/"+id);
   }
   
   clientsImgd(id){
    return   this.http.delete(environment.apiUrl +"/classifidesDetailsImg/"+id);
   }
   
   
   ClientsCatImgp(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesCatImg/"+id,data);
   }
   clientsImgCatIds(id){
    return this.http.get(environment.apiUrl +"/classifidesCatImg/"+id);
   }
   
   
   
   ClientsCatVidId(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesCatVid/"+id,data);
   }
   clientsVidCatId(id){
    return this.http.get(environment.apiUrl +"/classifidesCatVid/"+id);
   }
   
   blogsget(){
    return this.http.get(environment.apiUrl +"/clasdifiedsBlogs");
                }

  feedbackPosts(data) {
    return   this.http.post(environment.apiUrl +"/clasdifiedsFeedbacks",data);

 
    }
}