import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module' 
import { ClassifiedsdetailsComponent } from './classifiedsdetails.component';
import { ClassifiedsDetailsService } from './classifieds-details.service';

const routes:Routes=[{path:'details/:_id/:name',component:ClassifiedsdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [ClassifiedsdetailsComponent],
  providers: [ClassifiedsDetailsService],

})
export class ClassifiedsDetailsModule { }
