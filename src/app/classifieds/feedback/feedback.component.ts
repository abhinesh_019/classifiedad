import { Component, OnInit } from '@angular/core';
import { FeedbackService } from './feedback.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  feeds={
    "name":"",
    "mobileNo":"",
    "emailid":"",
    "subject":"",
    "description":""    
  }
  public aboutUsgets:any
  public classifiedExe:any
public executivesPersonsId:any
public blogsgets:any
  public formData: any = new FormData();
  constructor(private feedback:FeedbackService) { }

  ngOnInit() {
    this.getAboutBlogs()
this.getBlogs()
this.executivesGet()
}
executivesGet(){
  this.classifiedExe=localStorage.getItem('Executive  Id');
  this.feedback.executivesGets(this.classifiedExe).subscribe((res)=>{
    this.executivesPersonsId=res
     
      })
    }
  getAboutBlogs(){
    this.feedback.aboutUsget().subscribe((res)=>{
    this.aboutUsgets=res      
    }) 
   }
  feedBacks(){
 
    this.feedback.feedbackPosts(this.feeds).subscribe((res)=>{
    
     
      this.feeds.description="",
      this.feeds.emailid="",
      this.feeds.mobileNo="",
      this.feeds.name="",
      this.feeds.subject=""
      
    })  }
    getBlogs(){
      this.feedback.blogsget().subscribe((res)=>{
      this.blogsgets=res      
      })  }
   
}
