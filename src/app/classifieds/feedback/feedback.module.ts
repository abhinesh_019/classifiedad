import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module' ;
 import { FeedbackService } from './feedback.service';
 import { FeedbackComponent } from './feedback.component';

const routes:Routes=[{path:'Feedbacks',component:FeedbackComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [FeedbackComponent],
  providers: [FeedbackService],

})
export class FeedbackModule { }
