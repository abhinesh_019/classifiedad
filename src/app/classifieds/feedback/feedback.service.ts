import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class FeedbackService {

  constructor(private http: HttpClient) { }

  feedbackPosts(data) {
    return   this.http.post(environment.apiUrl +"/clasdifiedsFeedbacks",data);

 
    }
    aboutUsget(){
      return this.http.get(environment.apiUrl +"/clasdifiedsAboutUs");
                  }
                  executivesGets(id){
                    return this.http.get(environment.apiUrl +"/classifidesExecutives/"+id);
                   }
                  blogsget(){
                    return this.http.get(environment.apiUrl +"/clasdifiedsBlogs");
                                }
 
}
