import { Component, OnInit } from '@angular/core';
import { ClassifiedsCatageriesService } from './classifieds-catageries.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http'; 
import { map } from 'rxjs/operators'; import { Observable } from 'rxjs';



@Component({
  selector: 'app-classifiedscatageries',
  templateUrl: './classifiedscatageries.component.html',
  styleUrls: ['./classifiedscatageries.component.css']
})
export class ClassifiedscatageriesComponent implements OnInit {
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public localAreasId:any
  public localAreaData:any
public localAreaResults:any
public imagesCat:any
public catageriesLocalsData:any
public catageriesId:any
public catageriesd:any
public subCatageriesData:any
public subCatageriesId:any
public subCatageries:any
public datesGetData:any
public datesData:any
public datesId:any
public showmains:boolean=false
public selectedId:any
public executivesFieldsIds:any
public imagesOnes:any
public executivesFields:any
public executivesFieldsDeleteId:any
public executivesFieldsId:any
public executivesDelete:any
 public executivesDeleteId:any
public executivesPersons:any
public statusOneGetDetails:any
public statusOneDeleteId:any
public statusOnesId:any
public statusOnesName:any
public statusPoneId:any
public detailId:any
public catGroup:any
public areasAllGroup:any
public searchString:any
public ids:any
public catsCat:any
public catsId:any
public usersID:any
public locationsID:any
public areaID:any
public subAreaID:any
public catageriesLocalsDataAll:any
public locationsgetsId:any
public areasAllId:any
public localAreaDataid:any
public executivesPersonsId:any
public executiveId:any
public subCatagerie:any

 
public statusTwoGetDetails:any
public statusTwoDeleteId:any
public statusTwosId:any
public statusTwosName:any
public statusPTwoId:any
public clientsGetData:any
public detailsId:any
public aboutUsgets:any
public blogsgets:any
 // public catageriesLocalsDataAll:any
// public catageriesLocalsDataAll:any

feeds={
  "name":"",
  "mobileNo":"",
  "emailid":"",
  "subject":"",
  "description":""    
}
 
public formData: any = new FormData();



constructor(private classifieds:ClassifiedsCatageriesService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.areaID=localStorage.getItem('Area Id');
    this.executiveId=localStorage.getItem('Executive  Id');
    this.subAreaID=localStorage.getItem('Local Area Id');
    this.getAboutBlogs()
     this.getlocations()
    this.individualdata()
    this.getlocationsId()
    this.allAreasId()
    this.allAreas()
    this.localAreasIds()
    this.executivesGetId()
    this.getCatageriesLocalsAll()
    this.localAreas()
    this.getBlogs()

   }
 

  individualdata(){
    this.ids=this.route.snapshot.params['_id'];    
 this.getCatageriesLocals()
 this.subCatageriesGet()

 }

 
  getlocations(){

    this.classifieds.locationsget().subscribe((res)=>{
    this.locationsgets=res     
     
    var id =this.locationsgets[0];
   this.locations(id)
       
    }) 
   }
    locations(get?){
      this.locationName=get.locations
      this.its=get._id
      this.locationss=get.locations
      this.allAreas()
      this.allAreasGroup()
    }

    getlocationsId(){
      this.locationsID=localStorage.getItem('LocationsId');

      this.classifieds.locationsgetId(this.locationsID).subscribe((res)=>{
      this.locationsgetsId=res              
      })  
    }

    allAreas(){
      this.locationsID=localStorage.getItem('LocationsId');
       this.classifieds.areaapi(this.locationsID).subscribe((res)=>{
        this.areasAll=res
         var id =this.areasAll[0]; 
        this.selectedAreas(id)
       })
     
    }

    allAreasId(){
      this.areaID=localStorage.getItem('Area Id');
      this.classifieds.areaapid(this.areaID).subscribe((res)=>{
        this.areasAllId=res
        })
     
    }




    allAreasGroup(){
         
      this.classifieds.allAreasGroup(this.its).subscribe((res)=>{
        this.areasAllGroup=res        
      })
     
    }
    selectedAreas(result){
      this.selectAreaId=result._id
      this.selectArea=result.area 
      
       
    }
    
    
    localAreas(){
      this.areaID=localStorage.getItem('Area Id');

        this.classifieds.Localareaapi(this.areaID).subscribe((res)=>{
        this.localAreaData=res
         
        var id =this.localAreaData[0]; 
        this.selectedLocalAreas(id)
      })
    }
    selectedLocalAreas(result){
      this.localAreasId=result._id
      this.localAreaResults=result.localArea 
      

         }


    localAreasIds(){
      this.subAreaID=localStorage.getItem('Local Area Id');

      this.classifieds.Localareaapid(this.subAreaID).subscribe((res)=>{
        this.localAreaDataid=res 
       })
    }
   


       getCatageriesLocals(){
        
        this.classifieds.getCatageriesLocal(this.ids).subscribe((res)=>{
           this.catageriesLocalsData=res

            var id=this.catageriesLocalsData[0]
           this.getCatageriesClicks(id)
         })
      }
      getCatageriesLocalsAll(){
        this.subAreaID=localStorage.getItem('Local Area Id');
        this.classifieds.getCatageriesLocalAll(this.subAreaID).subscribe((res)=>{
           this.catageriesLocalsDataAll=res 
            var id=this.catageriesLocalsDataAll[0]
          })
      }
   
      getCatageriesClicks(result){
        this.catsId=result._id
        this.catsCat=result.classifiedsLocalArea 
          }
    //  ************************* EXECUTIVES PERSONS **************************
    executivesGetId(){
      this.executiveId=localStorage.getItem('Executive  Id');
      this.classifieds.executivesGetsId(this.executiveId).subscribe((res)=>{
        this.executivesPersonsId=res
         
        })
    
    }
    executivesGet(){
      this.subAreaID=localStorage.getItem('Local Area Id');

       this.classifieds.executivesGets(this.subAreaID).subscribe((res)=>{
        this.executivesPersons=res
         
        })
    
    }
    
    executivesExecutiveFieldsGet(get){
      this.executiveId=localStorage.getItem('Executive  Id');
       this.classifieds.executivesExecutiveFieldsGets(this.executiveId).subscribe((res)=>{
        this.executivesFields=res
        var id =this.executivesFields[0]    
       })
    }

    subCatageriesGet(){
      this.classifieds.subCatageriesg(this.ids).subscribe((res)=>{
        this.subCatageriesData=res
        var id =this.subCatageriesData[0]; 
        this.subCatageriesClicks(id)
      })
    }
    subCatageriesClicks(get?){
      this.subCatagerie=get.SubCatageries
      this.subCatageriesId=get._id
     this.datesGet()
     }


     datesGet(){
      this.classifieds.datesGets(this.subCatageriesId).subscribe((res)=>{
        this.datesGetData=res
         var id =this.datesGetData[0]; 
        this.datesClicks(id)
      })
    }
    datesClicks(get?){
      this.datesData=get.dates
      this.datesId=get._id
       this.clientsGet()
     }

     clientsGet(){

      var data:any =  {}
      if(this.searchString){
        data.search=this.searchString
        }
      this.classifieds.clientsg(this.datesId,data).subscribe((res)=>{
        this.clientsGetData=res 
         
        var id =this.clientsGetData[0]; 
 
        this.clientsGets(id)
       })
    }
    searchFilter(){
      this.clientsGet()      
    }
    clientsGets(get?){
       this.detailsId=get._id
       var id =this.detailsId[0]; 
   }


   mainMenuShowGet(items?){
    this.showmains=!this.showmains 
    this.selectedId=items
    this.classifieds.statusOneGetg(this.selectedId).subscribe((res)=>{
      this.statusOneGetDetails=res
   
       
      })
      this.classifieds.statusTwoGetg(this.selectedId).subscribe((res)=>{
        this.statusTwoGetDetails=res
    
          
        })
      }
      getBlogs(){
        this.classifieds.blogsget().subscribe((res)=>{
        this.blogsgets=res      
        })  }
      getAboutBlogs(){
        this.classifieds.aboutUsget().subscribe((res)=>{
        this.aboutUsgets=res      
        }) 
       }

 
}
