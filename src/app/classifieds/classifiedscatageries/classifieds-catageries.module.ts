import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
import { ClassifiedscatageriesComponent } from './classifiedscatageries.component';
import { ClassifiedsCatageriesService } from './classifieds-catageries.service';

const routes:Routes=[{path:'catageries/:_id/:name',component:ClassifiedscatageriesComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [ClassifiedscatageriesComponent],
  providers: [ClassifiedsCatageriesService],

})
export class ClassifiedsCatageriesModule { }
