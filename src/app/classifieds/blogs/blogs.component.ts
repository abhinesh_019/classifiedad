import { Component, OnInit } from '@angular/core';
import { BlogsService } from './blogs.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  public aboutUsgets:any;
public deleteId:any;


  formData: FormData = new FormData(); 
public imagesName:any
public imagesNames:any
public blogsgets:any
public deleteIds:any
public feedsGet:any
public classifiedExe:any
public executivesPersonsId:any
  constructor(private abouts:BlogsService) { }

  ngOnInit() {
    this.getBlogs()
    this.getAboutBlogs()
    this.executivesGet()
  }
  executivesGet(){
    this.classifiedExe=localStorage.getItem('Executive  Id');
    this.abouts.executivesGets(this.classifiedExe).subscribe((res)=>{
      this.executivesPersonsId=res
       
        })
      }
  getBlogs(){
    this.abouts.blogsget().subscribe((res)=>{
    this.blogsgets=res      
    })  }
    getAboutBlogs(){
      this.abouts.aboutUsget().subscribe((res)=>{
      this.aboutUsgets=res      
      }) 
     }

  
}
