import { Component, OnInit } from '@angular/core';
import { ClassifiedsGeneralsServicesService } from './classifieds-generals-services.service';

@Component({
  selector: 'app-classifiedsgenerals',
  templateUrl: './classifiedsgenerals.component.html',
  styleUrls: ['./classifiedsgenerals.component.css']
})
export class ClassifiedsgeneralsComponent implements OnInit {
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public localAreasId:any
  public localAreaData:any
public localAreaResults:any
public imagesCat:any
public catageriesLocalsData:any
public catageriesId:any
public catageriesd:any
public subCatageriesData:any
public subCatageriesId:any
public subCatageries:any
public datesGetData:any
public datesData:any
public datesId:any
public showmains:boolean=false
public selectedId:any
public executivesFieldsIds:any
public imagesOnes:any
public executivesFields:any
public executivesFieldsDeleteId:any
public executivesFieldsId:any
public executivesDelete:any
 public executivesDeleteId:any
public executivesPersons:any
public statusOneGetDetails:any
public statusOneDeleteId:any
public statusOnesId:any
public statusOnesName:any
public statusPoneId:any
public detailId:any
public catGroup:any
public areasAllGroup:any
public searchString:any
public executivesPersonsId:any
public aboutUsgets:any
public blogsgets:any
constructor(private classifieds:ClassifiedsGeneralsServicesService) { }

  ngOnInit() {
    this.getAboutBlogs()
    this.getlocations()
    this.getBlogs()

  }
  getAboutBlogs(){
    this.classifieds.aboutUsget().subscribe((res)=>{
    this.aboutUsgets=res      
    }) 
   }
  getlocations(){
    this.classifieds.locationsget().subscribe((res)=>{
    this.locationsgets=res 
    var id =this.locationsgets[0];

    
    this.locations(id)
       
    })  }
    locations(get?){
      this.locationName=get.locations
      this.its=get._id
      this.locationss=get.locations
      localStorage.setItem("LocationsId",this.its);

      this.allAreas() 
      this.allAreasGroup()
    }
    allAreas(){
         
      this.classifieds.areaapi(this.its).subscribe((res)=>{
        this.areasAll=res
        var id =this.areasAll[0]; 

        this.selectedAreas(id)
      })
     
    }
    allAreasGroup(){
         
      this.classifieds.allAreasGroup(this.its).subscribe((res)=>{
        this.areasAllGroup=res        
      })
     
    }
    selectedAreas(result){
      this.selectAreaId=result._id
      this.selectArea=result.area 
      localStorage.setItem("Area Id",this.selectAreaId);
      localStorage.setItem("Area Name",this.selectArea)

      this.localAreas()
    }
    
    
    localAreas(){
         
      this.classifieds.Localareaapi(this.selectAreaId).subscribe((res)=>{
        this.localAreaData=res
        var id =this.localAreaData[0]; 
        this.selectedLocalAreas(id)
        
      })
     
    }
    
    selectedLocalAreas(result){
      this.localAreasId=result._id
      this.localAreaResults=result.localArea 
      localStorage.setItem("Local Area Id",this.localAreasId);

      this.executivesGet()
      this.getCatageriesLocals()  
        }


       getCatageriesLocals(){
        var data:any =  {}
        if(this.searchString){
          data.search=this.searchString
          }
        this.classifieds.getCatageriesLocal(this.localAreasId,data).subscribe((res)=>{
           this.catageriesLocalsData=res
            
         })
      }
      searchFilter(){
        this.getCatageriesLocals()
        
      }
    //  ************************* EXECUTIVES PERSONS **************************
    executivesGet(){
      this.classifieds.executivesGets(this.localAreasId).subscribe((res)=>{
        this.executivesPersons=res
          
        var id =this.executivesPersons[0]; 
         
    
        this.executivesExecutiveFieldsGet(id)
       })
    
    }
    executivesExecutiveFieldsGet(get){
      this.executivesFieldsIds=get._id
      localStorage.setItem("Executive  Id",this.executivesFieldsIds);

      this.classifieds.executivesExecutiveFieldsGets(this.executivesFieldsIds).subscribe((res)=>{
        this.executivesFields=res
        var id =this.executivesFields[0]    
       })
    }
    getBlogs(){
      this.classifieds.blogsget().subscribe((res)=>{
      this.blogsgets=res      
      })  }
  
}
