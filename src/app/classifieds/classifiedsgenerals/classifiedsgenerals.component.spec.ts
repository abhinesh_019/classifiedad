import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiedsgeneralsComponent } from './classifiedsgenerals.component';

describe('ClassifiedsgeneralsComponent', () => {
  let component: ClassifiedsgeneralsComponent;
  let fixture: ComponentFixture<ClassifiedsgeneralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiedsgeneralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiedsgeneralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
