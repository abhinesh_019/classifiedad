import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { ClassifiedsgeneralsComponent } from './classifiedsgenerals.component';
import { ClassifiedsGeneralsServicesService } from './classifieds-generals-services.service';
import { ClassifiedsCatageriesModule } from '../classifiedscatageries/classifieds-catageries.module';
import { ClassifiedsDetailsModule } from '../classifiedsdetails/classifieds-details.module';
import { ClassifiedsDatesModule } from '../classifiedsdates/classifieds-dates.module';
import { AboutusModule } from '../about-us/aboutus.module';
import { ContactusModule } from '../contactus/contactus.module';
import { FeedbackModule } from '../feedback/feedback.module';
import { BlogsModule } from '../blogs/blogs.module';
const routes: Routes = [
  {
    path: 'classifieds', component: ClassifiedsgeneralsComponent, children:
      [
{ path: 'catageries/:_id/:name', loadChildren: 'app/classifieds/classifiedscatageries/classifieds-catageries.module#ClassifiedsCatageriesModule'},
{ path: 'details/:_id/:name', loadChildren: 'app/classifieds/classifiedsdetails/classifieds-details.module#ClassifiedsDetailsModule'},
{ path: 'imagesViews/:_id/:name', loadChildren: 'app/classifieds/classifiedsdates/classifieds-dates.module#ClassifiedsDatesModule'},
{ path: 'AboutUs', loadChildren: 'app/classifieds/about-us/aboutus.module#AboutusModule'},
{ path: 'ContactUS', loadChildren: 'app/classifieds/contactus/contactus.module#ContactusModule'},
{ path: 'Feedbacks', loadChildren: 'app/classifieds/feedback/feedback.module#FeedbackModule'},
{ path: 'Blogs', loadChildren: 'app/classifieds/blogs/blogs.module#BlogsModule'},

      ]
  }]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule,
    ClassifiedsCatageriesModule,
    ClassifiedsDetailsModule,
    ClassifiedsDatesModule,
    AboutusModule,
    ContactusModule,
    FeedbackModule,
    BlogsModule
  ],
  declarations: [ClassifiedsgeneralsComponent],
  providers: [ClassifiedsGeneralsServicesService],

})
export class ClassifiedsgeneralsModule { }



