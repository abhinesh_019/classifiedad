import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class ClassifiedsGeneralsServicesService {

  constructor(private http: HttpClient) { }
  locationsget(){
    return this.http.get(environment.apiUrl +"/clasdifiedsGeneralsLocations");
                }
                aboutUsget(){
                  return this.http.get(environment.apiUrl +"/clasdifiedsAboutUs");
                              }
                areaapi(id){
 
                 return this.http.get(environment.apiUrl +"/clasdifiedsGeneralsAreas/"+id);
               }
               allAreasGroup(id){
 
                return this.http.get(environment.apiUrl +"/clasdifiedsMainAreasGroup/"+id);
              }
               areapost(id,data){
                 return   this.http.post(environment.apiUrl +"/clasdifiedsGeneralsAreas/"+id,data);
                }

                Localareaapi(id){
 
                 return this.http.get(environment.apiUrl +"/clasdifiedsLocalAreas/"+id);
               }

               getCatageriesLocal(id,data){
    
                return this.http.get(environment.apiUrl +"/classifidesCatlocalArea/"+id,{params:data});
              }
              getCatageriesGropu(id){
    
                return this.http.get(environment.apiUrl +"/classifidesCatGroup/"+id);
              }
              postCatageriesLocalp(id,data){
                return   this.http.post(environment.apiUrl +"/classifidesCatlocalArea/"+id,data);
               }


               subCatageriesg(id){

                return this.http.get(environment.apiUrl +"/classifidsSubCat/"+id);
              }

              subCatageriesp(id,data){
                return   this.http.post(environment.apiUrl +"/classifidsSubCat/"+id,data);
               }
               datesGets(id){

                return this.http.get(environment.apiUrl +"/classifidesDetails/"+id);
              }

              datesPosts(id,data){
                return   this.http.post(environment.apiUrl +"/classifidesDetails/"+id,data);
               }

               executivesp(id,data){
                return   this.http.post(environment.apiUrl +"/classifidesExecutives/"+id,data);
               }
                
               executivesGets(id){
                return this.http.get(environment.apiUrl +"/classifidesExecutives/"+id);
               }
               
               executivesDelete(id){
                return   this.http.delete(environment.apiUrl +"/classifidesExecutives/"+id);
               }
            
               postExecutivesField(id,data){
                return   this.http.post(environment.apiUrl +"/classifidesExecutivesFields/"+id,data);
               }
               executivesExecutiveFieldsGets(id){
                return this.http.get(environment.apiUrl +"/classifidesExecutivesFields/"+id);
               }
               postExecutivesFieldsDelete(id){
                return   this.http.delete(environment.apiUrl +"/classifidesExecutivesFields/"+id);
               }
            
               blogsget(){
                return this.http.get(environment.apiUrl +"/clasdifiedsBlogs");
                            }

              feedbackPosts(data) {
                return   this.http.post(environment.apiUrl +"/clasdifiedsFeedbacks",data);
            
             
                }
}
