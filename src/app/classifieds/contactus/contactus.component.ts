import { Component, OnInit } from '@angular/core';
 import { ContactusService } from './contactus.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  public aboutUsgets:any;
public deleteId:any;


  formData: FormData = new FormData(); 
public imagesName:any
public imagesNames:any
public blogsgets:any
public deleteIds:any
public feedsGet:any
public classifiedExe:any
public executivesPersonsId:any
constructor(private abouts:ContactusService) { }

ngOnInit() {
  this.getBlogs()

  this.getAboutBlogs()
  this.executivesGet()
}
executivesGet(){
  this.classifiedExe=localStorage.getItem('Executive  Id');
  this.abouts.executivesGets(this.classifiedExe).subscribe((res)=>{
    this.executivesPersonsId=res
     
      })
    }
getAboutBlogs(){
  this.abouts.aboutUsget().subscribe((res)=>{
  this.aboutUsgets=res      
  })  }
  getBlogs(){
    this.abouts.blogsget().subscribe((res)=>{
    this.blogsgets=res      
    })  }
 
}

