import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { HttpClientModule } from '@angular/common/http'; 
import{ RouterModule,Routes} from'@angular/router';
import { AuthGaurdService } from './auth-gaurd.service';
 
const routes:Routes=[
  { path: '', redirectTo: 'classifieds/classifieds', pathMatch: 'full'},
   
    {
      path:'classifieds',
      loadChildren:'./classifieds/classifiedsgenerals/classifiedsgenerals.module#ClassifiedsgeneralsModule'
    },
    {
      path:'adminsClassifieds',
      loadChildren:'./adminsclassifieds/admins-classifieds/admins-classifieds.module#AdminsClassifiedsModule',
      canActivate:[AuthGaurdService]

    },
    {
      path:'login',
      loadChildren:'./login/main-module/main-module.module#MainModuleModule',
 
    },
    {
      path:'exeClassifieds',
      loadChildren:'./exe-classifieds/exe-classifieds/exe-classifieds.module#ExeClassifiedsModule',
      canActivate:[AuthGaurdService]

    },
]

@NgModule({
  declarations: [
    AppComponent
                ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
   
  ],
  providers: [AuthGaurdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
