import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
import { AdminsAboutBlogsFeedComponent } from './admins-about-blogs-feed.component';
import { AdminsAboutService } from './admins-about.service';

const routes:Routes=[{path:'adminsAbouts',component:AdminsAboutBlogsFeedComponent},]

 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule  ],
  declarations: [AdminsAboutBlogsFeedComponent],
  providers: [AdminsAboutService],

})
export class AdminsAboutModule { }
