import { Component, OnInit } from '@angular/core';
import { AdminsAboutService } from './admins-about.service';

@Component({
  selector: 'app-admins-about-blogs-feed',
  templateUrl: './admins-about-blogs-feed.component.html',
  styleUrls: ['./admins-about-blogs-feed.component.css']
})
export class AdminsAboutBlogsFeedComponent implements OnInit {
  public aboutUsgets:any;
public deleteId:any;


  formData: FormData = new FormData(); 
public imagesName:any
public imagesNames:any
public blogsgets:any
public deleteIds:any
public feedsGet:any

  about={
    "name":"",
    "address":"",
      "mobileNo":"",
     "whatsapp":"",
     "emailid":"",
     "fb":"",
     "inst":"",
   "youtube":"",
  "mainTitle":"",
  "subtitle":"",
  "description":"",
  "mainTitle1":"",
  "subtitle1":"",
  "description1":"",
  "mainTitle2":"",
  "subtitle2":"",
  "description2":"",
  "mainTitle3":"",
  "subtitle3":"",
  "description3":"",
  "image":""
  }
  blogs={
    "mainTitle":"",
    "subtitle":"",
    "description":"", 
    "image":"",
    "links":""
  }

  constructor(private abouts:AdminsAboutService) { }

  ngOnInit() {
    this.getAboutBlogs()
    this.getBlogs()
    this.feedback()
  }

// *********************catagories***************   
handleFile($event:any,image){
  this.imagesName= File = $event.target.files[0];
}

postAbout(){
 
 this.formData.append('image', this.imagesName);
 this.formData.append('name',this.about.name);
 this.formData.append('mobileNo',this.about.mobileNo);
 this.formData.append('whatsapp',this.about.whatsapp);
 this.formData.append('emailid',this.about.emailid);
 this.formData.append('address',this.about.address);
 this.formData.append('fb',this.about.fb);
 this.formData.append('inst',this.about.inst);
 this.formData.append('youtube',this.about.youtube);
 this.formData.append('mainTitle',this.about.mainTitle);
 this.formData.append('subtitle',this.about.subtitle);
 this.formData.append('description',this.about.description);
 this.formData.append('mainTitle1',this.about.mainTitle1);
 this.formData.append('subtitle1',this.about.subtitle1);
 this.formData.append('description1',this.about.description1);
 this.formData.append('mainTitle2',this.about.mainTitle2);
 this.formData.append('description2',this.about.description2);
 this.formData.append('subtitle2',this.about.subtitle2);
 this.formData.append('mainTitle3',this.about.mainTitle3);
 this.formData.append('subtitle3',this.about.subtitle3);
 this.formData.append('description3',this.about.description3);


 this.abouts.catpost(this.formData).subscribe((res)=>{
     
     })
      this.about.description="",
     this.about.description1="",
     this.about.description2="",
     this.about.description3="",
     this.about.emailid="",
     this.about.fb="",
      this.about.inst="",
      this.about.youtube="",
     this.about.mainTitle="",
     this.about.mainTitle1="",
     this.about.mainTitle2="",
     this.about.mainTitle3="",
     this.about.mobileNo="",
     this.about.name="",
     this.about.subtitle="",
     this.about.subtitle1="",
     this.about.subtitle2="",
     this.about.subtitle3="",
      this.about.image="",
      this.about.whatsapp="",
      this.about.address=""

     
}

getAboutBlogs(){
  this.abouts.aboutUsget().subscribe((res)=>{
  this.aboutUsgets=res      
  })  }

  deleteAboutUs(get){

    this.deleteId=get._id
     
     this.abouts.deleteAboutUs(this.deleteId).subscribe((res)=>{
        
     })
  }


  // *********************************************** BLOGS *******************************
 handleFileBlogs($event:any,image){
  this.imagesNames= File = $event.target.files[0];
}

postBlog(){
 
 this.formData.append('image', this.imagesNames);
         this.formData.append('mainTitle',this.blogs.mainTitle);
         this.formData.append('subtitle',this.blogs.subtitle);
         this.formData.append('description',this.blogs.description);
         this.formData.append('links',this.blogs.links);


 this.abouts.blogsPost(this.formData).subscribe((res)=>{
     
     })
      this.blogs.mainTitle="",
     this.blogs.subtitle="",
     this.blogs.description="",
     this.blogs.image="",
     this.blogs.links=""
      

     
}
getBlogs(){
  this.abouts.blogsget().subscribe((res)=>{
  this.blogsgets=res      
  })  }

  deleteBlog(get){

    this.deleteIds=get._id     
     this.abouts.deleteBlogs(this.deleteIds).subscribe((res)=>{
        console.log(res);
        
     })
  }

  feedback(){
    this.abouts.feedbacks().subscribe((res)=>{
    this.feedsGet=res      
    })  }
}
