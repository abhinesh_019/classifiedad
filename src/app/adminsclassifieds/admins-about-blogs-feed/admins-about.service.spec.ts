import { TestBed, inject } from '@angular/core/testing';

import { AdminsAboutService } from './admins-about.service';

describe('AdminsAboutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminsAboutService]
    });
  });

  it('should be created', inject([AdminsAboutService], (service: AdminsAboutService) => {
    expect(service).toBeTruthy();
  }));
});
