import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsAboutBlogsFeedComponent } from './admins-about-blogs-feed.component';

describe('AdminsAboutBlogsFeedComponent', () => {
  let component: AdminsAboutBlogsFeedComponent;
  let fixture: ComponentFixture<AdminsAboutBlogsFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsAboutBlogsFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsAboutBlogsFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
