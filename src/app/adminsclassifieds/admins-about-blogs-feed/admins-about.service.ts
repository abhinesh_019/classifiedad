import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()


export class AdminsAboutService {

  constructor(private http: HttpClient) { }
  catpost(data) {
    return   this.http.post(environment.apiUrl +"/clasdifiedsAboutUs",data);
 }
 aboutUsget(){
  return this.http.get(environment.apiUrl +"/clasdifiedsAboutUs");
              }
              deleteAboutUs(id){
                return   this.http.delete(environment.apiUrl +"/clasdifiedsAboutUs/"+id);
               }


               blogsPost(data) {
                return   this.http.post(environment.apiUrl +"/clasdifiedsBlogs",data);
             }     
             blogsget(){
              return this.http.get(environment.apiUrl +"/clasdifiedsBlogs");
                          }

                          deleteBlogs(id){
                            return   this.http.delete(environment.apiUrl +"/clasdifiedsBlogs/"+id);
                           }

                           feedbacks(){
                            return this.http.get(environment.apiUrl +"/clasdifiedsFeedbacks");
                                        }
}
