import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
import { AdminsClassifiedsImgsComponent } from './admins-classifieds-imgs.component';
import { AdminsClassifiedsImgService } from './admins-classifieds-img.service';

const routes:Routes=[{path:'adminsClientsImgs/:_id/:name',component:AdminsClassifiedsImgsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [AdminsClassifiedsImgsComponent],
  providers: [AdminsClassifiedsImgService],

})
export class AdminsClassifiedsImgModule { }
