import { Component, OnInit } from '@angular/core';
import { AdminsClassifiedsImgService } from './admins-classifieds-img.service';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-admins-classifieds-imgs',
  templateUrl: './admins-classifieds-imgs.component.html',
  styleUrls: ['./admins-classifieds-imgs.component.css']
})
export class AdminsClassifiedsImgsComponent implements OnInit {
public ids:any
public clientsCatImgs:any
public clientsCatVidRes:any
public clientsCatImgsId:any
public clientsImgsId:any
public detailsImgId:any
public detailsImgDelete:any
public detailsVidId:any
public detailsVidDelete:any


  public formData: any = new FormData();
  constructor(private classifieds:AdminsClassifiedsImgService,private route:ActivatedRoute) { }
  
    ngOnInit() {
   this.individualdata() 
   this.clientsImgId()
this.clientsImgCat()
     }
    individualdata(){
      this.ids=this.route.snapshot.params['_id'];    
   }


   clientsImgId(){
    this.classifieds.clientsImgId(this.ids).subscribe((res)=>{
      
      this.clientsImgsId=res 
console.log(res);

     })
  
  }
   clientsImgCat(){
    this.classifieds.clientsImgCatIds(this.ids).subscribe((res)=>{
      
      this.clientsCatImgs=res
      var id=this.clientsCatImgs[0]
      console.log(res);

     })
  
  }
 

  clientsDelete(get?){
    this.detailsImgId=get._id
     this.classifieds.clientsImgd(this.detailsImgId).subscribe((res)=>{
      this.detailsImgDelete=res 
  
     })
  }
 
}
