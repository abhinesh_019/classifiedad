import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminsClassifiedsImgService {

  constructor(private http: HttpClient) { }


  clientsVidCatId(id){
    return this.http.get(environment.apiUrl +"/classifidesCatVid/"+id);
   }
   clientsCatVidD(id){
    return   this.http.delete(environment.apiUrl +"/classifidesCatVid/"+id);
   }
   clientsImgId(id){
    return this.http.get(environment.apiUrl +"/classifidesDetailsImgId/"+id);
   }


   clientsImgCatIds(id){
    return this.http.get(environment.apiUrl +"/classifidesCatImg/"+id);
   }

   clientsImgd(id){
    return   this.http.delete(environment.apiUrl +"/classifidesCatImg/"+id);
   }

   
}
