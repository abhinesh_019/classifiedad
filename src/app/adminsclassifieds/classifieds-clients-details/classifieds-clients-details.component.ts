import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientsDetailsService } from './clients-details.service';

@Component({
  selector: 'app-classifieds-clients-details',
  templateUrl: './classifieds-clients-details.component.html',
  styleUrls: ['./classifieds-clients-details.component.css']
})
export class ClassifiedsClientsDetailsComponent implements OnInit {
public ids:any
public datesGetIds:any
public imagesOne:any
public executivesFields:any
public executivesFieldsId:any
 public executivesDelete:any
 public executivesDeleteId:any
public executivesPersons:any
public selectedId:any
public executivesFieldsIds:any
public imagesOnes:any 
public clientsCatImgs:any
public getViewsClientsImgId:any
public getViewsClientsImg:any
public clientsImgs:any
public clientsCatVidRes:any
public detailsImgId:any
public detailsImgDelete:any
// public clientsCatVidRes:any
// public clientsCatVidRes:any
public showmains:boolean=false
 




clients:{
  "name":"",
  "images":"",
  "description":""
}
 


clientsCatImg={
  "title":"",
  "images":"",
  "field1":"",
  "field2":"",
  "field3":"",
  "field4":"",
  "field5":"",
  "field6":"",
  "field7":"",
  "field8":"",
  "field9":"",
  "field10":"",
  "field11":"",
  "field12":""
}

 


public formData: any = new FormData();
constructor(private classifieds:ClientsDetailsService,private route:ActivatedRoute) { }

  ngOnInit() {
 this.individualdata()
 this.datesGetId()
 this.clientsImgCat()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];    
 }
 datesGetId(){
  this.classifieds.datesGets(this.ids).subscribe((res)=>{
    
    this.datesGetIds=res
    this.executivesGet()
  })

}
 

// ********************* Customer Details ****************

handleFileInput($event:any,images){
  this.imagesOne= File = $event.target.files[0];
 }


postDetailsLocals(){
this.formData.append('images',this.imagesOne);

this.formData.append('name',this.clients.name); 
this.formData.append('description',this.clients.description); 

  
 this.classifieds.executivesp(this.ids,this.formData).subscribe((res)=>{
     })
     this.clients.name="",
 
      this.clients.description="",
   
     this.clients.images=""

}
executivesGet(){
  this.classifieds.executivesGets(this.ids).subscribe((res)=>{
    this.executivesPersons=res
      var id =this.executivesPersons[0]; 
    this.executivesExecutiveFieldsGet(id)
    
    })
}
executivesExecutiveFieldsGet(get){
  this.executivesFieldsIds=get._id
  this.classifieds.executiveFieldsGets(this.executivesFieldsIds).subscribe((res)=>{
    this.executivesFields=res
    var id =this.executivesFields[0]    
   })
}
 
// ********************* Clients Cat Images ****************

handleFileInputClientsCatImg($event:any,images){
  this.imagesOnes= File = $event.target.files[0];
 }
  
postClientsCatImg(){
  // this.getViewsClientsImgId=clients._id

this.formData.append('images',this.imagesOnes); 

this.formData.append('title',this.clientsCatImg.title);   
this.formData.append('field1',this.clientsCatImg.field1);   
this.formData.append('field2',this.clientsCatImg.field2);   
this.formData.append('field3',this.clientsCatImg.field3);   
this.formData.append('field4',this.clientsCatImg.field4);   
this.formData.append('field5',this.clientsCatImg.field5);   
this.formData.append('field6',this.clientsCatImg.field6);   
this.formData.append('field7',this.clientsCatImg.field7);   
this.formData.append('field8',this.clientsCatImg.field8);   
this.formData.append('field9',this.clientsCatImg.field9);   
this.formData.append('field10',this.clientsCatImg.field10);   
this.formData.append('field11',this.clientsCatImg.field11);   
this.formData.append('field12',this.clientsCatImg.field12)

   
 this.classifieds.ClientsCatImgp(this.ids,this.formData).subscribe((res)=>{
     })
     this.clientsCatImg.title="", 
     this.clientsCatImg.images="",
     this.clientsCatImg.field1="",
     this.clientsCatImg.field2="",
     this.clientsCatImg.field3="",
     this.clientsCatImg.field4="",
     this.clientsCatImg.field5="",
     this.clientsCatImg.field6="",
     this.clientsCatImg.field7="",
     this.clientsCatImg.field8="",
     this.clientsCatImg.field9="",
     this.clientsCatImg.field10="",
     this.clientsCatImg.field11="",
     this.clientsCatImg.field12="" 
 
}

clientsImgCat(){
  this.classifieds.clientsImgCatIds(this.ids).subscribe((res)=>{
    
    this.clientsCatImgs=res
    var id=this.clientsCatImgs[0]

   })

}
 
clientsDelete(get?){
  this.detailsImgId=get._id
   this.classifieds.clientsImgd(this.detailsImgId).subscribe((res)=>{
    this.detailsImgDelete=res 

   })
}
}
