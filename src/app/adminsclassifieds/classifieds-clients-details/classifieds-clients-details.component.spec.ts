import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiedsClientsDetailsComponent } from './classifieds-clients-details.component';

describe('ClassifiedsClientsDetailsComponent', () => {
  let component: ClassifiedsClientsDetailsComponent;
  let fixture: ComponentFixture<ClassifiedsClientsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiedsClientsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiedsClientsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
