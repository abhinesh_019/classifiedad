import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
import { ClassifiedsClientsDetailsComponent } from './classifieds-clients-details.component';
import { ClientsDetailsService } from './clients-details.service';

const routes:Routes=[{path:'adminsClientsDetails/:_id/:name',component:ClassifiedsClientsDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [ClassifiedsClientsDetailsComponent],
  providers: [ClientsDetailsService],

})
export class ClassifiedsClientsDetailsModule { }
