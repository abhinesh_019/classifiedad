import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminsClassifiedsDetailsService {

  constructor(private http: HttpClient) { }
  
  subCatageriesg(id){
    
    return this.http.get(environment.apiUrl +"/classifidsSubCat/"+id);
  }

  subCatageriesp(id,data){
    return   this.http.post(environment.apiUrl +"/classifidsSubCat/"+id,data);
   }
   datesGets(id){

    return this.http.get(environment.apiUrl +"/classifidesDate/"+id);
  }

  datesPosts(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesDate/"+id,data);
   }

   clientsp(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesDetails/"+id,data);
   }
   clientsg(id){
    return   this.http.get(environment.apiUrl +"/classifidesDetails/"+id);
   }
   clientsd(id){
    return   this.http.delete(environment.apiUrl +"/classifidesDetails/"+id);
   }
   
}

