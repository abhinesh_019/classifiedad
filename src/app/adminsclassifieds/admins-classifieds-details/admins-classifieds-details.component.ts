import { Component, OnInit } from '@angular/core';
import { AdminsClassifiedsDetailsService } from './admins-classifieds-details.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admins-classifieds-details',
  templateUrl: './admins-classifieds-details.component.html',
  styleUrls: ['./admins-classifieds-details.component.css']
})
export class AdminsClassifiedsDetailsComponent implements OnInit {
  
  public catageriesId:any
  public catageriesd:any
  public subCatageriesData:any
  public subCatageriesId:any
  public subCatageries:any
  public datesGetData:any
  public datesData:any
  public datesId:any
  public imagesOne:any
  public imagesTwo:any
  public imagesThree:any
  public imagesFour:any
  public imagesFive:any
  public vie:any
  public clientsGetData:any
// public SubCats:any
  public ids:any



  subCat={
    "catageries":"",
    "SubCatageries":""
   }
   dat={
    "dates":"" 
  }
clients={
  "name":"",
  "dates":"",
  "mobileNo":"",
  "emailid":"",
  "address":"",
  "mainTitle":"",
  "subTitle":"",
  "subTitleOne":"",
  "description":"",
  "images1":"",
  "images2":"",
  "images3":"",
  "images4":"",
  "images5":"",
  "description1":"",
  "description2":"",
  "description3":"",
  "description4":"",
  "description5":""  
}
  public formData: any = new FormData();

  constructor(private classifieds:AdminsClassifiedsDetailsService,private route:ActivatedRoute) { }

  ngOnInit() { 
    this.individualdata()
    this.subCatageriesGet()

  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}
// *********************Sub - Catageries ****************
 
subCatageriesPost(){
  this.classifieds.subCatageriesp(this.ids,this.subCat).subscribe((res)=>{
     this.subCat.catageries=this.catageriesd,
     this.subCat.SubCatageries=""

  }) 
}
subCatageriesGet(){
  this.classifieds.subCatageriesg(this.ids).subscribe((res)=>{
    this.subCatageriesData=res
    var id =this.subCatageriesData[0]; 
    this.subCatageriesClicks(id)
  })
}
subCatageriesClicks(get?){
  this.subCatageries=get.SubCatageries
  this.subCatageriesId=get._id
  
 this.datesGet()
 }
 // ********************* Dates *************
 datesPost(){
  this.classifieds.datesPosts(this.subCatageriesId,this.dat).subscribe((res)=>{

     this.dat.dates=""
   }) 
}
datesGet(){
  this.classifieds.datesGets(this.subCatageriesId).subscribe((res)=>{
    this.datesGetData=res
    var id =this.datesGetData[0]; 
    this.datesClicks(id)
  })
}
datesClicks(get?){
  this.datesData=get.dates
  this.datesId=get._id
  var id =this.datesData[0]; 
 }

}
