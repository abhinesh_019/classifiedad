import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsClassifiedsDetailsComponent } from './admins-classifieds-details.component';

describe('AdminsClassifiedsDetailsComponent', () => {
  let component: AdminsClassifiedsDetailsComponent;
  let fixture: ComponentFixture<AdminsClassifiedsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsClassifiedsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsClassifiedsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
