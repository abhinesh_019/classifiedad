import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
import { AdminsClassifiedsDetailsComponent } from './admins-classifieds-details.component';
import { AdminsClassifiedsDetailsService } from './admins-classifieds-details.service';

const routes:Routes=[{path:'adminsDetails/:_id/:name',component:AdminsClassifiedsDetailsComponent},]

 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule  ],
  declarations: [AdminsClassifiedsDetailsComponent],
  providers: [AdminsClassifiedsDetailsService],

})
export class AdminsClassifiedsDetailsModule { }
