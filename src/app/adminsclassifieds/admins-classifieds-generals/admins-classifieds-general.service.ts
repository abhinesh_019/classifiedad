import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminsClassifiedsGeneralService {

  constructor(private http: HttpClient) { }
  datesGets(id){

    return this.http.get(environment.apiUrl +"/classifidesDateId/"+id);
  }

  clientsp(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesDetails/"+id,data);
   }
   clientsg(id,data){
    return   this.http.get(environment.apiUrl +"/classifidesDetails/"+id,{params:data});
   }
   clientsd(id){
    return   this.http.delete(environment.apiUrl +"/classifidesDetails/"+id);
   }
   

   clientsStatusOneP(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesStatusOne/"+id,data);
   }
   statusOneGetg(id){
    return this.http.get(environment.apiUrl +"/classifidesStatusOne/"+id);
   }
   statusOneDelete(id){
    return   this.http.delete(environment.apiUrl +"/classifidesStatusOne/"+id);
   }

   clientsStatusTwoP(id,data){
    return   this.http.post(environment.apiUrl +"/classifidesStatusTwo/"+id,data);
   }
   statusTwoGetg(id){
    return this.http.get(environment.apiUrl +"/classifidesStatusTwo/"+id);
   }
   statusTwoDelete(id){
    return   this.http.delete(environment.apiUrl +"/classifidesStatusTwo/"+id);
   }
}

