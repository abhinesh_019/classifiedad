import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminsClassifiedsGeneralsComponent } from './admins-classifieds-generals.component';
import { AdminsClassifiedsGeneralService } from './admins-classifieds-general.service';
 
const routes:Routes=[{path:'adminsGenerals/:_id/:name',component:AdminsClassifiedsGeneralsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule 
  ],
  declarations: [AdminsClassifiedsGeneralsComponent],
  providers: [AdminsClassifiedsGeneralService],

})
export class AdminsClassifiedsGeneralModule { }
