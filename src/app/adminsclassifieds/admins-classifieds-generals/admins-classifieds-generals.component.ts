import { Component, OnInit } from '@angular/core';
import { AdminsClassifiedsGeneralService } from './admins-classifieds-general.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http'; 
import { map } from 'rxjs/operators'; import { Observable } from 'rxjs';


@Component({
  selector: 'app-admins-classifieds-generals',
  templateUrl: './admins-classifieds-generals.component.html',
  styleUrls: ['./admins-classifieds-generals.component.css']
})
export class AdminsClassifiedsGeneralsComponent implements OnInit {
  public imagesOne:any
  public imagesTwo:any
  public imagesThree:any
  public imagesFour:any
  public imagesFive:any
  public vie:any
  public clientsGetData:any
public detailsId:any
  public clientsDeletes:any
  public ids:any
public datesGetIds:any
public searchString:any
public detailsVid:any
public showmains:boolean=false
public selectedId:any
public executivesFieldsIds:any
public imagesOnes:any
public executivesFields:any
public executivesFieldsDeleteId:any
public executivesFieldsId:any
public executivesDelete:any
 public executivesDeleteId:any
public executivesPersons:any
public statusOneGetDetails:any
public statusOneDeleteId:any
public statusOnesId:any
public statusOnesName:any
public statusPoneId:any
public detailId:any


public statusTwoGetDetails:any
public statusTwoDeleteId:any
public statusTwosId:any
public statusTwosName:any
public statusPTwoId:any
  
 statusOne={
   "statusOne":""
 }
 statusTwo={
  "statusTwo":""
}
  clients={
    "name":"",
    "mobileNo":"",
    "emailid":"",
    "Hno":"",
    "streetNo":"",
    "colony":"",
    "area":"",
    "city":"",
    "pincode":"",
    "whatsApp":"",
    "mainTitle":"",
    "subTitle":"",
     "description":"",
    "images1":""
    
  }
  public formData: any = new FormData();
  constructor(private http: HttpClient,private classifieds:AdminsClassifiedsGeneralService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.individualdata()

    this.datesGetId()

  }
  
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];    
    this.clientsGet()

 }
 datesGetId(){
  this.classifieds.datesGets(this.ids).subscribe((res)=>{
    this.datesGetIds=res
     
   })
}

// ********************* Customer Details ****************

  handleFileInput($event:any,images){
    this.imagesOne= File = $event.target.files[0];
   }
   
  
  postDetailsLocals(){
  this.formData.append('images1',this.imagesOne); 

  this.formData.append('name',this.clients.name); 
  this.formData.append('mobileNo',this.clients.mobileNo); 
  this.formData.append('emailid',this.clients.emailid); 
  this.formData.append('Hno',this.clients.Hno); 
  this.formData.append('streetNo',this.clients.streetNo); 
  this.formData.append('colony',this.clients.colony); 
  this.formData.append('area',this.clients.area); 
  this.formData.append('city',this.clients.city); 
  this.formData.append('pincode',this.clients.pincode); 
 
  this.formData.append('mainTitle',this.clients.mainTitle); 
  this.formData.append('subTitle',this.clients.subTitle); 
  this.formData.append('whatsApp',this.clients.whatsApp); 
  this.formData.append('description',this.clients.description)
    
   this.classifieds.clientsp(this.ids,this.formData).subscribe((res)=>{
       })
       this.clients.name="",
       this.clients.mobileNo="",
       this.clients.emailid="",
       this.clients.Hno="",
       this.clients.streetNo="",
       this.clients.colony="",
       this.clients.area="",
       this.clients.city="",
       this.clients.pincode="",

       this.clients.mainTitle="",
       this.clients.subTitle="",

       this.clients.whatsApp="",
       this.clients.description="",
       this.clients.images1="" 
       
  }
 


  clientsGet(){
    var data:any =  {}
    if(this.searchString){
      data.search=this.searchString
      }
    this.classifieds.clientsg(this.ids,data).subscribe((res)=>{
      this.clientsGetData=res 
      var id =this.clientsGetData[0]; 
      this.clientsGets(id)
     })
  }
  searchFilter(){
    this.clientsGet()
    
  }
  clientsGets(get?){
     this.detailsId=get._id
     var id =this.detailsId[0]; 
 
 }
  clientsDelete(get?){
    this.detailsId=get._id
    var id =this.detailsId[0]; 
    this.classifieds.clientsd(this.detailsId).subscribe((res)=>{
      this.clientsDeletes=res 

     })
  }

  mainMenuShow(items?){
    this.showmains=!this.showmains 
    this.selectedId=items
      }

// ********************* Status One ****************
 
clientsStatusOne(isg){
  this.statusPoneId=isg
  this.classifieds.clientsStatusOneP(this.statusPoneId,this.statusOne).subscribe((res)=>{
      this.statusOne.statusOne=""

  }) 
}

mainMenuShowGet(items?){
  this.showmains=!this.showmains 
  this.selectedId=items
  this.classifieds.statusOneGetg(this.selectedId).subscribe((res)=>{
    this.statusOneGetDetails=res

     
    })
    this.classifieds.statusTwoGetg(this.selectedId).subscribe((res)=>{
      this.statusTwoGetDetails=res       
      })
    }
statusOneDeletes(get){
  this.statusOneDeleteId=get._id
   
   this.classifieds.statusOneDelete(this.statusOneDeleteId).subscribe((res)=>{
  
   })
}


// ********************* Status Two ****************
 
clientsStatusTwo(isg){
  this.statusPTwoId=isg
  this.classifieds.clientsStatusTwoP(this.statusPTwoId,this.statusTwo).subscribe((res)=>{
      this.statusTwo.statusTwo=""

  }) 
}
 
statusTwoDeletes(get){
  this.statusTwoDeleteId=get._id
   
   this.classifieds.statusTwoDelete(this.statusTwoDeleteId).subscribe((res)=>{
  
   })
}

}
 

