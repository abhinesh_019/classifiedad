import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { AdminsClassifiedsComponent } from './admins-classifieds.component';
import { AdminsClassifiedsService } from './admins-classifieds.service';
import { AdminsClassifiedsDetailsModule } from '../admins-classifieds-details/admins-classifieds-details.module';
import { AdminsClassifiedsGeneralModule } from '../admins-classifieds-generals/admins-classifieds-general.module';
import { ClassifiedsClientsDetailsModule } from '../classifieds-clients-details/classifieds-clients-details.module';
import { AdminsClassifiedsImgModule } from '../admins-classifieds-imgs/admins-classifieds-img.module';
import { AdminsAboutModule } from '../admins-about-blogs-feed/admins-about.module';
import { AdminsImpNotesModule } from '../admins-imp-notes/admins-imp-notes.module';
import { AdminadminDeleteAccModule } from '../admin-delete-acc/adminadmin-delete-acc.module';

const routes: Routes = [
  {
    path: '', component: AdminsClassifiedsComponent, children:
      [
      { path: 'adminsDetails/:_id/:name', loadChildren: 'app/adminsclassifieds/admins-classifieds-details/admins-classifieds-details.module#AdminsClassifiedsDetailsModule'},
      { path: 'adminsGenerals/:_id/:name', loadChildren: 'app/adminsclassifieds/admins-classifieds-generals/admins-classifieds-general.module#AdminsClassifiedsGeneralModule'},
      { path: 'adminsClientsDetails/:_id/:name', loadChildren: 'app/adminsclassifieds/classifieds-clients-details/classifieds-clients-details.module#ClassifiedsClientsDetailsModule'},
      { path: 'adminsClientsImgs/:_id/:name', loadChildren: 'app/adminsclassifieds/admins-classifieds-imgs/admins-classifieds-img.module#AdminsClassifiedsImgModule'},
      { path: 'adminsAbouts', loadChildren: 'app/adminsclassifieds/admins-about-blogs-feed/admins-about.module#AdminsAboutModule'},
      { path: 'adminsImpnotes', loadChildren: 'app/adminsclassifieds/admins-imp-notes/admins-imp-notes.module#AdminsImpNotesModule'},
      { path: 'adminsDeleteAcc', loadChildren: 'app/adminsclassifieds/admin-delete-acc/adminadmin-delete-acc.module#AdminadminDeleteAccModule'},

    ]
  }]
  

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule,
    AdminsClassifiedsDetailsModule,
    AdminsClassifiedsGeneralModule,
    ClassifiedsClientsDetailsModule,
    AdminsClassifiedsImgModule,
    AdminsAboutModule,
    AdminsImpNotesModule,
    AdminadminDeleteAccModule
  ],
  declarations: [AdminsClassifiedsComponent],
  providers: [AdminsClassifiedsService],

})
export class AdminsClassifiedsModule { }
