import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsClassifiedsComponent } from './admins-classifieds.component';

describe('AdminsClassifiedsComponent', () => {
  let component: AdminsClassifiedsComponent;
  let fixture: ComponentFixture<AdminsClassifiedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsClassifiedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsClassifiedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
