import { Component, OnInit } from '@angular/core';
import { AdminsClassifiedsService } from '../../adminsclassifieds/admins-classifieds/admins-classifieds.service';
@Component({
  selector: 'app-admins-classifieds',
  templateUrl: './admins-classifieds.component.html',
  styleUrls: ['./admins-classifieds.component.css']
})
export class AdminsClassifiedsComponent implements OnInit {
  locationsdata={
    "locations":""
  }
  areas={
    "area":"" 
  }
  lAreas={
    "localArea":""
  }
  catageriesLocals={
    "catageries":"",
    "images":""
  }
  subCat={
    "catageries":"",
    "SubCatageries":""
   }
   dat={
    "dates":""
  }
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public localAreasId:any
  public localAreaData:any
public localAreaResults:any
public imagesCat:any
public catageriesLocalsData:any
public catageriesId:any
public catageriesd:any
public subCatageriesData:any
public subCatageriesId:any
public subCatageries:any
public datesGetData:any
public datesData:any
public datesId:any
public showmains:boolean=false
public selectedId:any
public executivesFieldsIds:any
public imagesOnes:any
public executivesFields:any
public executivesFieldsDeleteId:any
public executivesFieldsId:any
public executivesDelete:any
 public executivesDeleteId:any
public executivesPersons:any
public statusOneGetDetails:any
public statusOneDeleteId:any
public statusOnesId:any
public statusOnesName:any
public statusPoneId:any
public detailId:any
public userID:any

executives={
  "name":"",
  "mobileNo":"",
  "emailID":"",
  "description":"",
  "images":""
}
 exeFields={
   "fields":""
 }


  public formData: any = new FormData();
  constructor(private classifieds:AdminsClassifiedsService) { }

  ngOnInit() {
    this.getlocations()
  }
  postlocations(){
 
    this.classifieds.locationspost(this.locationsdata).subscribe((res)=>{
    
     
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.classifieds.locationsget().subscribe((res)=>{
      this.locationsgets=res 
      var id =this.locationsgets[0];
 
      
      this.locations(id)
         
      })  }
      locations(get?){
        this.locationName=get.locations
        this.its=get._id
        this.locationss=get.locations
      
        this.allAreas() 
      }
      allAreas(){
           
        this.classifieds.areaapi(this.its).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0]; 
          this.selectedAreas(id)
          
        })
       
      }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area 
        this.localAreas()
      }
      
      postarea(){
        this.classifieds.areapost(this.its,this.areas).subscribe((res)=>{
           this.areas.area=""
        }) 
      }

      postLocalarea(){
        this.classifieds.Localareapost(this.selectAreaId,this.lAreas).subscribe((res)=>{
           this.lAreas.localArea=""
           
        }) 
      }
      localAreas(){
           
        this.classifieds.Localareaapi(this.selectAreaId).subscribe((res)=>{
          this.localAreaData=res
          var id =this.localAreaData[0]; 
          this.selectedLocalAreas(id)
          
        })
       
      }
      
      selectedLocalAreas(result){
        this.localAreasId=result._id
        this.localAreaResults=result.localArea 
        this.executivesGet()
        this.getCatageriesLocals()
      }

 // ********************* Catageries - Local Areas****************

 handleFileInput($event:any,images){
  this.imagesCat= File = $event.target.files[0];
 }
 

postCatageriesLocals(){
this.formData.append('images',this.imagesCat);
this.formData.append('catageries',this.catageriesLocals.catageries); 
 
 this.classifieds.postCatageriesLocalp(this.localAreasId,this.formData).subscribe((res)=>{
     })
     this.catageriesLocals.catageries="",
     this.catageriesLocals.images=""
}
getCatageriesLocals(){
  this.classifieds.getCatageriesLocal(this.localAreasId).subscribe((res)=>{
     this.catageriesLocalsData=res
      
   })
}

 
// ********************* Executives ****************

handleFileInputExecuties($event:any,images){
  this.imagesOnes= File = $event.target.files[0];
 }
  
postExecutives(){
this.formData.append('images',this.imagesOnes); 

this.formData.append('name',this.executives.name);  
this.formData.append('mobileNo',this.executives.mobileNo);  
this.formData.append('emailID',this.executives.emailID);  

 this.formData.append('description',this.executives.description); 
   
 this.classifieds.executivesp(this.localAreasId,this.formData).subscribe((res)=>{
     })
     this.executives.name="", 
      this.executives.description="",
      this.executives.mobileNo="",
      this.executives.emailID="",
     this.executives.images=""
  
     
}


executivesGet(){
  this.classifieds.executivesGets(this.localAreasId).subscribe((res)=>{
    this.executivesPersons=res
    var id =this.executivesPersons[0]; 
     

    this.executivesExecutiveFieldsGet(id)
   })

}
deleteExecutives(get){

  this.executivesDeleteId=get._id
   
   this.classifieds.executivesDelete(this.executivesDeleteId).subscribe((res)=>{
    this.executivesDelete=res
     
   })
}

userAccountIdDelete(){
  this.userID=localStorage.getItem('ads');
   
  this.classifieds.executivesUserDelete(this.executivesDeleteId).subscribe((res)=>{
     
  })
}

postExecutivesFields(get){
  this.executivesFieldsId=get._id
  this.classifieds.postExecutivesField(this.executivesFieldsId,this.exeFields).subscribe((res)=>{
    this.exeFields.fields=""
    
 }) 
}
executivesExecutiveFieldsGet(get){
  this.executivesFieldsIds=get._id
  this.classifieds.executivesExecutiveFieldsGets(this.executivesFieldsIds).subscribe((res)=>{
    this.executivesFields=res
    var id =this.executivesFields[0]    
   })
}
postExecutivesFieldsDelete(result){
  this.executivesFieldsDeleteId=result._id
  this.classifieds.postExecutivesFieldsDelete(this.executivesFieldsDeleteId).subscribe((res)=>{
   })
}

 
}
