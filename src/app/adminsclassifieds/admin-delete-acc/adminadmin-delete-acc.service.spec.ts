import { TestBed, inject } from '@angular/core/testing';

import { AdminadminDeleteAccService } from './adminadmin-delete-acc.service';

describe('AdminadminDeleteAccService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminadminDeleteAccService]
    });
  });

  it('should be created', inject([AdminadminDeleteAccService], (service: AdminadminDeleteAccService) => {
    expect(service).toBeTruthy();
  }));
});
