import { Component, OnInit } from '@angular/core';
import { AdminadminDeleteAccService } from './adminadmin-delete-acc.service';

@Component({
  selector: 'app-admin-delete-acc',
  templateUrl: './admin-delete-acc.component.html',
  styleUrls: ['./admin-delete-acc.component.css']
})
export class AdminDeleteAccComponent implements OnInit {
public accNoDeleteId:any
public accDetails:any
public searchString:any
  constructor(private classifieds:AdminadminDeleteAccService) { }

  ngOnInit() {
    this.userAccountId()
  }
  userAccountIdDelete(result?){
    this.accNoDeleteId=result._id
    this.classifieds.executivesUserDelete(this.accNoDeleteId).subscribe((res)=>{
       
    })
  }

  userAccountId(){
     this.classifieds.userAccountIds().subscribe((res)=>{
       this.accDetails=res
    })
  }

  // userAccountId(){
  //   var data:any =  {}
  //   if(this.searchString){
  //     data.search=this.searchString
  //     }
  //     this.classifieds.userAccountIds(data).subscribe((res)=>{
  //       this.accDetails=res
  //       console.log(res);
        
  //      })
  // }
  // searchFilter(){
  //   this.userAccountId()
    
  // }
 

}
