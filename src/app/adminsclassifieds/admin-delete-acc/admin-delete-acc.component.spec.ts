import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDeleteAccComponent } from './admin-delete-acc.component';

describe('AdminDeleteAccComponent', () => {
  let component: AdminDeleteAccComponent;
  let fixture: ComponentFixture<AdminDeleteAccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDeleteAccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDeleteAccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
