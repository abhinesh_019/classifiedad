import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module'
 import { AdminsImpNotesService } from './admins-imp-notes.service';
import { AdminsImpNotesComponent } from './admins-imp-notes.component';

const routes:Routes=[{path:'adminsImpnotes',component:AdminsImpNotesComponent},]

 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule  ],
  declarations: [AdminsImpNotesComponent],
  providers: [AdminsImpNotesService],

})
export class AdminsImpNotesModule { }
