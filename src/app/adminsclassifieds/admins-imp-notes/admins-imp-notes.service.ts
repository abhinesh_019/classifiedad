import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()


export class AdminsImpNotesService {

  constructor(private http: HttpClient) { }
  postImps(data) {
    return   this.http.post(environment.apiUrl +"/clasdifiedsImpNotes",data);
 }
 impNote(){
  return this.http.get(environment.apiUrl +"/clasdifiedsImpNotes");
              }
              deleteImps(id){
                return   this.http.delete(environment.apiUrl +"/clasdifiedsImpNotes/"+id);
               }

}
