import { TestBed, inject } from '@angular/core/testing';

import { AdminsImpNotesService } from './admins-imp-notes.service';

describe('AdminsImpNotesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminsImpNotesService]
    });
  });

  it('should be created', inject([AdminsImpNotesService], (service: AdminsImpNotesService) => {
    expect(service).toBeTruthy();
  }));
});
