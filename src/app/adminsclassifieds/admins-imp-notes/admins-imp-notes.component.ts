import { Component, OnInit } from '@angular/core';
import { AdminsImpNotesService } from './admins-imp-notes.service';

@Component({
  selector: 'app-admins-imp-notes',
  templateUrl: './admins-imp-notes.component.html',
  styleUrls: ['./admins-imp-notes.component.css']
})
export class AdminsImpNotesComponent implements OnInit {
  public aboutUsgets:any;
public deleteId:any;


  formData: FormData = new FormData(); 
public imagesName:any
public imagesNames:any
public impsgets:any
public deleteIds:any
public feedsGet:any

  impnotes={
    "name":"",
    "mobileNo":"",
    "whatsapp":"", 
    "mainTitle":"",
    "image":"",
    "subtitle":"",
    "description":"",
  }

  constructor(private imp:AdminsImpNotesService) { }

  ngOnInit() {
     this.impNotes()
   }
 handleFileBlogs($event:any,image){
  this.imagesNames= File = $event.target.files[0];
}

postImp(){
 
 this.formData.append('image', this.imagesNames);
         this.formData.append('mainTitle',this.impnotes.mainTitle);
         this.formData.append('subtitle',this.impnotes.subtitle);
         this.formData.append('description',this.impnotes.description);
         this.formData.append('name',this.impnotes.name);
         this.formData.append('whatsapp',this.impnotes.whatsapp);
         this.formData.append('mobileNo',this.impnotes.mobileNo);


 this.imp.postImps(this.formData).subscribe((res)=>{
     
     })
      this.impnotes.mainTitle="",
     this.impnotes.subtitle="",
     this.impnotes.description="",
     this.impnotes.image="" ,
     this.impnotes.name="" ,
     this.impnotes.whatsapp="" ,
     this.impnotes.mobileNo="" 

     
}
impNotes(){
  this.imp.impNote().subscribe((res)=>{
  this.impsgets=res      
  })  }

  deleteImp(get){

    this.deleteIds=get._id     
     this.imp.deleteImps(this.deleteIds).subscribe((res)=>{
         
     })
  }
}
