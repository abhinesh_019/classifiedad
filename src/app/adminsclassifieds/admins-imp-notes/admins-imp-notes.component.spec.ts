import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsImpNotesComponent } from './admins-imp-notes.component';

describe('AdminsImpNotesComponent', () => {
  let component: AdminsImpNotesComponent;
  let fixture: ComponentFixture<AdminsImpNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsImpNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsImpNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
