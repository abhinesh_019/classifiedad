import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";

@Injectable()
export class AuthGaurdService implements CanActivate{
constructor(private router:Router){}

  canActivate ():boolean{
    if(localStorage.getItem('accesstoken')){
        return true;
    }else{
      this.router.navigate(['/login/login'])
        return false;
    }
}
} 
